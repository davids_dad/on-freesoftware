#Magical thinking
“Any sufficiently advanced technology is indistinguishable from magic.” —  Arthur C. Clarke
When we are not free to use, study, modify, and share, then we give up understanding, and see the product as magic.