###UNIX started free
UNIX was shared at AT&T, but then became proprietary. 
One copy went to Berkeley with a troubling None-Disclosure-Agreement (that made the signatories unemployable), 
but then AT&T released everyone from it. 
Another copy got soled to businesses that sold copies (selling copies it OK, nothing wrong with that, 
the Free Software encourages it: Software is not free unless you are free to sell copies).

The Berkeley version became BSD. 
The Proprietary version has mostly disappeared in to obscurity. Gnu and Linux (Linux is just the kernel), 
was written from scratch, it is not derived from UNIX. It is however based on Unix/Posix standards and specifications.

###UNIX was expensive
Yes that is why most people bought Microsoft's DOS and then windows (it was cheaper).