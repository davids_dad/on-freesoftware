----

----

##Introduction

I have just been reading the [latest “Hello, World”](https://helloworld.raspberrypi.org/issues/6). I am pleased to see that it is focused on ethics.

I note that it mentions freedom only 2 times. Freedom of users is very important.


##What do you want, from your computer?

- Do you use computers in the classroom?
- Do you want to be able to login quickly?
- Do you want it to be easy to use?, powerful?, reliable?, secure?, free from viruses?, not hang?
- Do you teach how they work?
- Do you teach programing?
- Do you want to install the latest operating system and applications, on all of your computers?
- Do you want to use your hard-disk to store your data? (not the operating system)?
- Do you want your computer to remain cold, and not waste the battery?
- Do you want to be able to change your supplier?
- Do you want to not worry about software licences?
- Do you want to run the fastest growing operating system (my popularity), and that is used by the top 500 super-computers, 60% of the web, most of the internet, many embedded systems, and the raspberry-pi?
- Do you want students to share the software that they write?
- Do you want to own the software that your students write, without taking ownership from them?

If any of the above apply to you, then read on.

##Software Freedom

We as user need freedom to use, study, distribute, and modify our software. 
If we do not have these freedoms, then we can not solve the other issues.

There are many trying to take away our freedom. We use free-services, on the internet, but are they free? We are allowed to use these services, because we are not the customer, we are the product. Our data is being sold, we are being spied on, we are being manipulated.

If we are consumers of products, as opposed to customers that are in charge of how we use the product, then we are giving up control to others.
This gives the developers of the software power over us, and takes away our power to authenticaly live our lives.


##Freedom
It is not enough to be able to program, we need to be free to program.
However this freedom does not benefit only those that can program. It benefits all.
We all benefit my improvements made by others, and we can ask a programmer to make changes for us.

-----------------

Here is an edited version of the post we deleted (The computer would not let us edit the original, so we deleted it).

###Free as in freedom.
I am using free to mean freedom, not free of charge. Freedom is not against money. You can sell copies of Free Software. 


###On making a living
This is only about software developers (like me), not about the majority.
Freedom is for as all. Giving people freedom, 
will disadvantage privileged people.
However even the most privilaged benefit in a more equal society.

If in your current job, you can not make money without taking away the freedom of others, then get another job.