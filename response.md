@Paul you say that the Free Software Foundation has a very wide definition of commercial.

Yes as in:
 - Free to use, modify, copy, distribute  **for any purpose**.
 - Commercial-use ∈ Any-purpose.
 - Commercial-use is a member of the set Any-purpose.

So they bang on about how commercial is allowed, is is just one more thing that is allowed. They only mention it, because so many people say, incorrectly that commercial use is not allowed. And some people write non-free licences with a non-commercial clause (such as the CC-NC licences). To be Free Software it must allow use in a commercial environment (the no discrimination clause), and allow it to be sold (the free distribution clause).

@Ian you keep going on about money, this is not what Free means. We care not about the money. We should be willing to pay extra for Free Software.

You ask why any one would give there stuff away. (Yes they give it away to the community, though they still remain copyright holder. This allows dual licencing. And the license stops others taking it from them.) (“If you have an apple and I have an apple and we exchange these apples, then you and I will still each have one apple. But if you have an idea and I have an idea and we exchange these ideas, then each of us will have two ideas.” ― George Bernard Shaw ) This is a very good question, with many answers. Hero are some of them.

 - To give something back
 - To reduce maintenance costs.
 - To improve some software, that they use.
 - As a charity, “it is the right thing to do”.
 - Is a condition of research grant.
 - Customer demands it.
 - To get experience: As an intern I mould be expected to hand over my creations to the company, and not get paid. If the software is Free, then I also get to keep it.

There are many other reasons. If we were to ask this when it all started, we may think that we could never succeed. However we now see that money is not the main motivator. We also see that money is not the best motivator (studies have shown that money is a poor motivator, it motivates for people to turn-up. But is a poor motivator for quality.) Now we can look back at history, and see that we have something better than proprietary software. So we must stop asking if it is possible, but only ask how.

But all this talk of why a someone/some-organisation should write Free Software is a distraction. I am only saying that we should use Free Software. Most people use software, few write it, even fewer make money from writing it. For most software is a cost, not a source of profit. We need to help them reduce there costs (most of the cost of software comes from the shackles, not the price).

-------------
I don't have all of the answers.

 I am only writing about Free Software. Yes patents cause problems else-ware as well. But I don't think they are fundamentally broken in other domains. However as you point out the current implementation of he patent system is broken, but could be fixed. (Patents for software has no benefits, and many many harms.)

Patents are a monopoly, so a 30 year patent is a 30 year monopoly. Nothing can build on it for 30 years, prices can be artificially high for 30 years.

However could we have a system where patents are more clever. A discussion is had, about the selling price, the investment, etc, and the duration of the patent. If a company puts in a lot of investment, and/or promises to keep prices low they would be given a longer patent. Patents should be conditionally extendable.