##What do you want, from your computer?

- Do you have a computer?
- Do you want your computer to work for you?
- Do you want it to be easy to use?, powerful?, reliable?, secure?, free from viruses?, not hang?
- Do you want to learn how it works? and to learn to program?
- Do you want to install the latest operating system and applications, on all of your computers?
- Do you want to use your hard-disk to store your data? (not the operating system)?
- Do you want your computer to remain cold, and not waste the battery?
- Do you want to run the fastest growing operating system (my popularity), and that is used by the top 500 super-computers, 60% of the web, most of the internet, many embedded systems, and the raspberry-pi?
- Do you want to own the software that you write?
- Do you want to share the software that you write?
- Do you want to be able to change your supplier?
- Do you want to not worry about software licences?

If any of the above apply to you, then read on.